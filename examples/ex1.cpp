#include <iostream>
#include "gui.hpp"

int main(int argc, char** argv) {
    gui::init(argc, argv);

    gui::window window("Hello", 400, 200);
    window.setBorder(10);
    window.signal("delete-event", gui::quit);

    gui::grid grid;
    window.add(grid);

    gui::input input;
    grid.add({gui::label("Type something: "), input, gui::button("Close window", gui::close, window)});

    gui::button button("Print content", [](GtkWidget* w) {
        std::cout << gui::input(w).text() << std::endl;
    }, input);
    grid.add(button, 1);

    window.show();
    gui::main();
}
