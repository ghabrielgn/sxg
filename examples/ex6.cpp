#include <iostream>
#include "gui.hpp"

gui::window* window;

void test1() {
    std::cout << "Open" << std::endl;
    std::string filename = gui::dialog::open(*window, "Open File", "Open");
    if (filename != "") {
        std::cout << "Opening " << filename << "..." << std::endl;
    }
}

void test2() {
    std::cout << "Save" << std::endl;
    std::string filename = gui::dialog::save(*window, "Save File", "Save");
    if (filename != "") {
        std::cout << "Saving " << filename << "..." << std::endl;
    }
}

void test3() {
    std::cout << "Clear" << std::endl;
}

int main(int argc, char** argv) {
    gui::init(argc, argv);

    window = new gui::window("Hello", 500, 500);
    window->signal("delete-event", gui::quit);

    gui::box mainbox(gui::box::VERTICAL);
    gui::box outerbox(gui::box::HORIZONTAL, 15);
    outerbox.setBorder(10);
    gui::box sidebox(gui::box::VERTICAL, 3);

    gui::menubar menu;
    menu.add("File", {{"Open", test1}, {"Save", test2}});
    menu.add("Objects", {{"Clear", test3}});
    mainbox << menu;

    gui::list objList;
    sidebox.add({gui::label("List"), gui::scroll_area(200, 200, GTK_SHADOW_IN).add(objList)});

    gui::hierarchy(*window, mainbox, outerbox, sidebox);

    window->show();
    gui::main();
    delete window;
}
