#include <iostream>
#include "gui.hpp"

int main(int argc, char** argv) {
    gui::init(argc, argv);

    gui::window window("Hello", 400, 400);
    window.setBorder(10);
    window.signal("delete-event", gui::quit);

    gui::grid grid;
    window.add(grid);

    grid.add({gui::button("A"), gui::button("B")});
    grid.add(gui::button("C"), 2, 0, 2);
    grid.add(gui::button("D"), 4);
    grid.add({gui::button("A2"), gui::button("B2"), gui::button("C2"), gui::button("D2")}, 1);

    window.show();
    gui::main();
}
