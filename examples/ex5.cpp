#include <iostream>
#include "gui.hpp"

int main(int argc, char** argv) {
    gui::init(argc, argv);

    gui::window window("Hello", 200, 200);
    window.setBorder(10);
    window.signal("delete-event", gui::quit);

    gui::list list;
    for (unsigned i = 0; i < 100; i++) {
        list << "Test " + std::to_string(i);
    }

    window << gui::scroll_area(200, 200, gui::SHADOW_IN).add(list);
    window.show();

    gui::main();
}
