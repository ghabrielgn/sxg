#include <iostream>
#include "gui.hpp"

int main(int argc, char** argv) {
    gui::init(argc, argv);

    gui::window window("Hello", 400, 400);
    window.setBorder(10);
    window.signal("delete-event", gui::quit);

    gui::grid grid;
    window.add(grid);

    gui::canvas canvas(300, 300);
    grid.add(canvas);
    grid.add(gui::button("Draw (first)", [](GtkWidget* w) {
        gui::canvas(w).strokeRect(100, 100, 50, 50);
    }, canvas), 1);

    gui::canvas canvas2(200, 200);
    grid.add(canvas2, 2);
    grid.add(gui::button("Draw (second)", [](GtkWidget* w) {
        auto canvas = gui::canvas(w);
        canvas.setColor(255, 0, 0);
        // Examples:
        //canvas.fillRect(50, 50, 70, 40);
        //canvas.strokePolygon({{75, 50}, {100, 75}, {100, 25}});
        //canvas.strokeCircle(75, 75, 50);

        canvas.arc(75, 75, 50);
        canvas.moveTo(110, 75);
        canvas.arc(75, 75, 35, 0, M_PI);
        canvas.moveTo(65, 65);
        canvas.arc(60, 65, 5);
        canvas.moveTo(95, 65);
        canvas.arc(90, 65, 5);
        canvas.stroke();
    }, canvas2), 3);

    window.show();
    gui::main();
}
