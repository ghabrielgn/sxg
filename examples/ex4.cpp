#include <iostream>
#include "gui.hpp"

int main(int argc, char** argv) {
    gui::init(argc, argv);

    gui::window window("Hello", 400, 400);
    window.setBorder(10);
    window.signal("delete-event", gui::quit);

    gui::box box(gui::box::HORIZONTAL);
    window.add(box);

    box.add({{gui::label("A")}, {gui::label("B")}, {gui::label("C")}});
    box.add(gui::label("D"));

    window.show();
    gui::main();
}
