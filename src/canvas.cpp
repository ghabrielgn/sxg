#include "canvas.hpp"

namespace {
    std::unordered_map<GtkWidget*, cairo_t*> cr;
    std::unordered_map<GtkWidget*, cairo_surface_t*> surface;

    void canvas_alloc(GtkWidget* widget) {
        cr.insert(std::make_pair(widget, nullptr));
        surface.insert(std::make_pair(widget, nullptr));
    }

    void canvas_clear(cairo_t* _cr) {
        cairo_set_source_rgb(_cr, 1, 1, 1);
        cairo_paint(_cr);
        cairo_set_source_rgb(_cr, 0, 0, 0);
    }

    bool canvas_config(GtkWidget* widget) {
        if (surface[widget])
            cairo_surface_destroy(surface[widget]);

        if (cr[widget])
            cairo_destroy(cr[widget]);

        surface[widget] = gdk_window_create_similar_surface(
                            gtk_widget_get_window(widget),
                            CAIRO_CONTENT_COLOR,
                            gtk_widget_get_allocated_width(widget),
                            gtk_widget_get_allocated_height(widget));

        cr[widget] = cairo_create(surface[widget]);
        canvas_clear(cr[widget]);
        return true;
    }

    void canvas_draw(GtkWidget* widget, cairo_t* _cr, gpointer data) {
        cairo_set_source_surface(_cr, surface[widget], 0, 0);
        cairo_paint(_cr);
    }
}

gui::canvas::canvas(unsigned width, unsigned height) : canvas(gtk_drawing_area_new()) {
    setSize(width, height);
    canvas_alloc(obj);
    g_signal_connect(obj, "configure-event", G_CALLBACK(canvas_config), NULL);
    g_signal_connect(obj, "draw", G_CALLBACK(canvas_draw), NULL);
}

void gui::canvas::buildRectangle(double x, double y, unsigned width, unsigned height) {
    cairo_rectangle(cairo(), x, y, width, height);
}

void gui::canvas::buildPolygon(const std::initializer_list<std::pair<double, double>>& pairs) {
    for (auto& pair : pairs) {
        cairo_line_to(cairo(), pair.first, pair.second);
    }
    cairo_close_path(cairo());
}

void gui::canvas::setSize(unsigned width, unsigned height) {
    gtk_widget_set_size_request(obj, width, height);
}

cairo_t* gui::canvas::cairo() const {
    return cr[obj];
}

void gui::canvas::setColor(unsigned r, unsigned g, unsigned b, double a) {
    cairo_set_source_rgba(cairo(), r/255.0, g/255.0, b/255.0, a);
}

void gui::canvas::fill(bool enqueue) {
    cairo_fill(cairo());
    if (enqueue) {
        queue();
    }
}

void gui::canvas::stroke(bool enqueue) {
    cairo_stroke(cairo());
    if (enqueue) {
        queue();
    }
}

void gui::canvas::queue() {
    gtk_widget_queue_draw(obj);
}

void gui::canvas::moveTo(double x, double y) {
    cairo_move_to(cairo(), x, y);
}

void gui::canvas::strokeRect(double x, double y, unsigned width, unsigned height) {
    buildRectangle(x, y, width, height);
    stroke();
}

void gui::canvas::fillRect(double x, double y, unsigned width, unsigned height) {
    buildRectangle(x, y, width, height);
    fill();
}

void gui::canvas::strokePolygon(const std::initializer_list<std::pair<double, double>>& pairs) {
    buildPolygon(pairs);
    stroke();
}

void gui::canvas::fillPolygon(const std::initializer_list<std::pair<double, double>>& pairs) {
    buildPolygon(pairs);
    fill();
}

void gui::canvas::arc(double x, double y, unsigned radius, double start, double end, bool clockwise) {
    if (clockwise) {
        cairo_arc(cairo(), x, y, radius, start, end);
    } else {
        cairo_arc_negative(cairo(), x, y, radius, start, end);                
    }
}

void gui::canvas::strokeCircle(double x, double y, unsigned radius) {
    arc(x, y, radius);
    stroke();
}

void gui::canvas::fillCircle(double x, double y, unsigned radius) {
    arc(x, y, radius);
    fill();
}

void gui::canvas::strokeArc(double x, double y, unsigned radius, double start, double end, bool clockwise) {
    arc(x, y, radius, start, end, clockwise);
    stroke();
}
