#include <iostream>
#include "gui.hpp"

gui::list* objList;

void test() {
    objList->add("Test");
}

int main(int argc, char** argv) {
    gui::init(argc, argv);

    gui::window window("Hello", 500, 500);
    window.setBorder(10);
    window.signal("delete-event", gui::quit);

    gui::box outerbox(gui::box::HORIZONTAL, 15);
    gui::box sidebox(gui::box::VERTICAL, 3);
    objList = new gui::list();
    sidebox.add({gui::label("Objects list"), gui::scroll_area(200, 200, GTK_SHADOW_IN).add(*objList)});

    gui::frame objFrame("Create Object", 0.5);
    gui::grid objGrid(3, 3, true, true);
    objGrid.setBorder(5);
    objGrid.add(gui::button("Point", test), 0, 0, 2);
    objGrid.add(gui::button("Line"), 2, 0, 2);
    objGrid.add(gui::button("Polygon"), 1, 1, 2);

    gui::frame winFrame("Window", 0.5);
    gui::box winBox(gui::box::VERTICAL);
    winBox.setBorder(3);
    gui::frame navFrame("Navigation", 0.5);
    gui::grid navGrid(3, 3, true, true);
    navGrid.setBorder(5);
    navGrid.add(gui::button("\u25B2"), 1)
           .add(gui::button("\u25C0"), 0, 1)
           .add(gui::button("\u25B6"), 2, 1)
           .add(gui::button("\u25BC"), 1, 2);

    gui::frame zoomFrame("Zoom", 0.5);
    gui::box zoomBox(gui::box::HORIZONTAL, 1);
    zoomBox.setBorder(3);
    zoomBox.add({gui::button("+"), gui::button("-"), gui::input("5").align(1).limit(3, 3),
                 gui::label("%"), gui::button("Set")});

    gui::hierarchy(window, outerbox, sidebox);
    gui::hierarchy(sidebox, objFrame, objGrid);
    gui::hierarchy(sidebox, winFrame, winBox, navFrame, navGrid);
    gui::hierarchy(winBox, zoomFrame, zoomBox);

    window.show();
    gui::main();
}
