#include "grid.hpp"

gui::grid::grid(unsigned rowSpacing, unsigned colSpacing, bool rowHomog, bool colHomog)
: grid(gtk_grid_new()) {
    setSpacing(rowSpacing, colSpacing);
    setHomogeneous(rowHomog, colHomog);
}

gui::grid& gui::grid::add(const std::initializer_list<widget>& widgets, unsigned row) {
    unsigned i = 0;
    for (auto& widget : widgets) {
        gtk_grid_attach(inst, widget.object(), i++, row, 1, 1);
    }
    return *this;
}

gui::grid& gui::grid::add(const widget& widget, unsigned col, unsigned row, unsigned colspan, unsigned rowspan) {
    gtk_grid_attach(inst, widget.object(), col, row, colspan, rowspan);
    return *this;
}

gui::grid& gui::grid::setSpacing(unsigned rowSpacing, unsigned colSpacing) {
    gtk_grid_set_row_spacing(inst, rowSpacing);
    gtk_grid_set_column_spacing(inst, colSpacing);
    return *this;
}

gui::grid& gui::grid::setHomogeneous(bool rowHomog, bool colHomog) {
    gtk_grid_set_row_homogeneous(inst, rowHomog);
    gtk_grid_set_column_homogeneous(inst, colHomog);
    return *this;
}
