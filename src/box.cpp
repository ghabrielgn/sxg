#include "box.hpp"

gui::box::box(bool orientation, unsigned spacing)
: box(gtk_box_new(orientation == HORIZONTAL ? GTK_ORIENTATION_HORIZONTAL : GTK_ORIENTATION_VERTICAL, spacing)) {}

gui::box& gui::box::add(const std::vector<box_pack>& packs) {
    for (auto pack : packs) {
        gtk_box_pack_start(inst, pack.widget.object(), pack.alloc, pack.expand, pack.offset);
    }
    return *this;
}

gui::box& gui::box::add(const std::initializer_list<widget>& list) {
    for (auto& widget : list) {
        gtk_box_pack_start(inst, widget.object(), false, false, 0);
    }
    return *this;
}

gui::box& gui::box::add(const widget& widget) {
    return add({box_pack(widget)});
}

gui::box& gui::box::setHomogeneous(bool homogeneous) {
    gtk_box_set_homogeneous(inst, homogeneous);
    return *this;
}
