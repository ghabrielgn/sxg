#include "button.hpp"

gui::button::button(const std::string& text) : button(gtk_button_new_with_label(text.c_str())) {}

gui::button::button(const std::string& text, void (*fn)()) : button(text) {
    signal("clicked", fn);
}

gui::button::button(const std::string& text, void (*fn)(GtkWidget*), widget& target) : button(text) {
    swappedSignal("clicked", fn, target);
}
