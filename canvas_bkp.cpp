#ifndef GUI_CANVAS_HPP
#define GUI_CANVAS_HPP

#include <iostream>
#include "widget.hpp"

namespace {
    cairo_t* cr = NULL;
    cairo_surface_t*  surface = NULL;

    void canvas_clear() {
        cairo_set_source_rgb(cr, 1, 1, 1);
        cairo_paint(cr);
        cairo_set_source_rgb(cr, 0, 0, 0);
    }

    bool canvas_config(GtkWidget* widget) {
        if (surface)
            cairo_surface_destroy(surface);

        if (cr)
            cairo_destroy(cr);

        surface = gdk_window_create_similar_surface(
                    gtk_widget_get_window(widget),
                    CAIRO_CONTENT_COLOR,
                    gtk_widget_get_allocated_width(widget),
                    gtk_widget_get_allocated_height(widget));

        cr = cairo_create(surface);

        canvas_clear();
        return true;
    }

    void canvas_draw_aux(cairo_t* _cr) {
        cairo_set_source_surface(_cr, surface, 0, 0);
        cairo_paint(_cr);
    }

    void canvas_draw(GtkWidget* widget, cairo_t* _cr, gpointer data) {
        canvas_draw_aux(_cr);
    }
}

namespace gui {
    class canvas : public gui::widget {
    private:
        unsigned id;

    public:
        canvas(GtkWidget* widget) {
            obj = widget;
        }

        canvas(unsigned width = 200, unsigned height = 200)
        : canvas(gtk_drawing_area_new()) {
            setSize(width, height);
            id = canvas_alloc();
            g_signal_connect(obj, "configure-event", G_CALLBACK(canvas_config), NULL);
            g_signal_connect(obj, "draw", G_CALLBACK(canvas_draw), NULL);
        }

        void setSize(unsigned width, unsigned height) {
            gtk_widget_set_size_request(obj, width, height);
        }

        void stroke() {
            cairo_stroke(cr);
        }

        void queue() {
            gtk_widget_queue_draw(obj);
        }

        void strokeRect(double x, double y, unsigned width, unsigned height) {
            cairo_rectangle(cr, x, y, width, height);
            stroke();
            queue();
        }
    };
};

#endif