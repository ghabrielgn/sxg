#ifndef GUI_MENU_ITEM_HPP
#define GUI_MENU_ITEM_HPP

#include "container.hpp"

namespace gui {
    class menu_item : public gui::container {
    public:
        menu_item(GtkWidget* widget) {
            obj = widget;
            inst = GTK_MENU_ITEM(obj);
        }

        menu_item(const std::string& text = "")
        : menu_item(gtk_menu_item_new_with_mnemonic(text.c_str())) {}

    private:
        GtkMenuItem* inst;
    };
};

#endif