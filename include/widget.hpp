#ifndef GUI_WIDGET_HPP
#define GUI_WIDGET_HPP

#include <gtk/gtk.h>
#include <string>

namespace gui {
    class widget {
    public:
        virtual ~widget() {}

        template<typename T>
        void signal(const std::string& name, T (*fn)()) {
            g_signal_connect(obj, name.c_str(), G_CALLBACK(fn), NULL);
        }

        template<typename T>
        void signal(const std::string& name, T (*fn)(widget&)) {
            g_signal_connect(obj, name.c_str(), G_CALLBACK(fn), NULL);
        }

        //void signal(const std::string& name, const basic_pack& pack);

        template<typename T>
        void swappedSignal(const std::string& name, T (*fn)(GtkWidget*), widget& target) {
            g_signal_connect_swapped(obj, name.c_str(), G_CALLBACK(fn), target.object());
        }

        GtkWidget* object() const {
            return obj;
        }

        void show() const {
            gtk_widget_show_all(obj);
        }

    protected:
        GtkWidget* obj;
        widget() {}
    };
};

#endif