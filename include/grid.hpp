#ifndef GUI_GRID_HPP
#define GUI_GRID_HPP

#include <initializer_list>
#include "container.hpp"

namespace gui {
    class grid : public gui::container {
    public:
        grid(GtkWidget* widget) {
            obj = widget;
            inst = GTK_GRID(obj);
        }

        grid(unsigned = 0, unsigned = 0, bool = false, bool = false);
        grid& add(const std::initializer_list<widget>&, unsigned = 0);
        grid& add(const widget&, unsigned = 0, unsigned = 0, unsigned = 1, unsigned = 1);
        grid& setSpacing(unsigned = 0, unsigned = 0);
        grid& setHomogeneous(bool = true, bool = true);


        /*template<typename... Args>
        grid& add(const widget& widget, Args... others) {
            return add(0, 0, widget, others...);
        }

        template<typename... Args>
        grid& add(unsigned row, const widget& widget, Args... others) {
            return add(0, row, widget, others...);
        }*/

    private:
        GtkGrid* inst;

        /*template<typename... Args>
        grid& add(unsigned col, unsigned row, const widget& widget, Args... others) {
            add(widget, col, row);
            return add(col + 1, row, others...);
        }

        grid& add(unsigned, unsigned) {
            return *this;
        }*/
    };
};

#endif