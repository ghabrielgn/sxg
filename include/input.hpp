#ifndef GUI_INPUT_HPP
#define GUI_INPUT_HPP

#include "widget.hpp"

namespace gui {
    class input : public gui::widget {
    public:
        input(GtkWidget* widget) {
            obj = widget;
            inst = GTK_ENTRY(obj);
        }

        input(const std::string& content = "") : input(gtk_entry_new()) {
            text(content);
        }

        std::string text() const {
            return gtk_entry_get_text(inst);
        }

        input& text(const std::string& newText) {
            gtk_entry_set_text(inst, newText.c_str());
            return *this;
        }

        input& align(double alignment) {
            gtk_entry_set_alignment(inst, alignment);
            return *this;
        }

        input& limit(unsigned numChars, unsigned visibleChars) {
            gtk_entry_set_max_length(inst, numChars);
            gtk_entry_set_width_chars(inst, visibleChars);
            gtk_entry_set_max_width_chars(inst, visibleChars);
            return *this;
        }

    private:
        GtkEntry* inst;
    };
};

#endif