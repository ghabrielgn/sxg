#ifndef GUI_DIALOG_HPP
#define GUI_DIALOG_HPP

#include <initializer_list>
#include "container.hpp"
#include "window.hpp"

namespace gui {
    class dialog : public gui::container {
    public:
        dialog(GtkWidget* widget) {
            obj = widget;
            inst = GTK_DIALOG(obj);
        }

        static std::string open(const gui::window& window, const std::string& title, const std::string& okLabel) {
            return file(GTK_FILE_CHOOSER_ACTION_OPEN, window, title, okLabel);
        }

        static std::string save(const gui::window& window, const std::string& title, const std::string& okLabel) {
            return file(GTK_FILE_CHOOSER_ACTION_SAVE, window, title, okLabel);
        }

        static std::string file(const GtkFileChooserAction& action, const gui::window& window,
            const std::string& title,const std::string& okLabel) {
            auto dialog = gtk_file_chooser_dialog_new(title.c_str(), GTK_WINDOW(window.object()),
                action, "Cancel", GTK_RESPONSE_CANCEL,
                okLabel.c_str(), GTK_RESPONSE_ACCEPT, NULL);

            if (action == GTK_FILE_CHOOSER_ACTION_SAVE) {
                gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), true);
            }

            auto response = gtk_dialog_run(GTK_DIALOG(dialog));
            std::string result;
            if (response == GTK_RESPONSE_ACCEPT) {
                char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
                result = filename;
                g_free(filename);
            }

            gtk_widget_destroy(dialog);
            return result;
        }

    private:
        GtkDialog* inst;
    };
};

#endif