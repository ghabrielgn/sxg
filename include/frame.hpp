#ifndef GUI_FRAME_HPP
#define GUI_FRAME_HPP

#include <vector>
#include "container.hpp"

namespace gui {
    class frame : public gui::container {
    public:
        frame(GtkWidget* widget) {
            obj = widget;
            inst = GTK_FRAME(obj);
        }
        
        frame(const std::string& text, double xalign = 0, double yalign = 0.5)
        : frame(gtk_frame_new(text.c_str())) {
            align(xalign, yalign);
        }

        frame& align(double x, double y) {
            gtk_frame_set_label_align(inst, x, y);
            return *this;
        }

    private:
        GtkFrame* inst;
    };
};

#endif