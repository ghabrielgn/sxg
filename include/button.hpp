#ifndef GUI_BUTTON_HPP
#define GUI_BUTTON_HPP

#include "widget.hpp"

namespace gui {
    class button : public gui::widget {
    public:
        button(GtkWidget* widget) {
            obj = widget;
        }

        button(const std::string&);
        button(const std::string& text, void (*)());
        button(const std::string& text, void (*)(GtkWidget*), widget&);
    };
};

#endif