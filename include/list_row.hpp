#ifndef GUI_LIST_ROW_HPP
#define GUI_LIST_ROW_HPP

#include <vector>
#include "container.hpp"

namespace gui {
    class list_row : public gui::container {
    public:
        list_row(GtkWidget* widget) {
            obj = widget;
            inst = GTK_LIST_BOX_ROW(obj);
        }

        list_row(GtkListBoxRow* row) : inst(row) {}

    private:
        GtkListBoxRow* inst;
    };
};

#endif