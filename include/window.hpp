#ifndef GUI_WINDOW_HPP
#define GUI_WINDOW_HPP

#include "container.hpp"

namespace gui {
    class window : public gui::container {
    public:
        window(GtkWidget* widget) {
            obj = widget;
            inst = GTK_WINDOW(obj);
        }

        window(unsigned width = 200, unsigned height = 200)
        : window(gtk_window_new(GTK_WINDOW_TOPLEVEL)) {
            setSize(width, height);
        }

        window(const std::string& title, unsigned width = 200, unsigned height = 200) : window(width, height) {
            setTitle(title);
        }

        void setSize(unsigned width, unsigned height) {
            gtk_window_set_default_size(inst, width, height);
        }

        void setTitle(const std::string& title) {
            gtk_window_set_title(inst, title.c_str());
        }

    private:
        GtkWindow* inst;
    };
};

#endif