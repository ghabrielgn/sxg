#ifndef GUI_CONTAINER_HPP
#define GUI_CONTAINER_HPP

#include "widget.hpp"

namespace gui {
    class container : public gui::widget {
    public:
        void setBorder(unsigned value) {
            gtk_container_set_border_width(inst(), value);
        }

        virtual container& add(const widget& widget) {
            gtk_container_add(inst(), widget.object());
            return *this;
        }

        virtual container& operator<<(const widget& widget) {
            return add(widget);
        }

    protected:
        container() {}
        GtkContainer* inst() {
            return GTK_CONTAINER(obj);
        }
    };
};

#endif
