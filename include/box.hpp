#ifndef GUI_BOX_HPP
#define GUI_BOX_HPP

#include <initializer_list>
#include <vector>
#include "container.hpp"

namespace {
    struct box_pack {
        box_pack(const gui::widget& w, bool a, bool e = false, int o = 0)
        : widget(w), alloc(a), expand(e), offset(o) { }
        box_pack(const gui::widget& w, int o = 0)
        : widget(w), alloc(false), expand(false), offset(o) { }
        const gui::widget& widget;
        bool alloc;
        bool expand;
        int offset;
    };
}

namespace gui {
    class box : public gui::container {
    public:
        static const bool HORIZONTAL = 0;
        static const bool VERTICAL = 1;

        box(GtkWidget* widget) {
            obj = widget;
            inst = GTK_BOX(obj);
        }

        box(bool, unsigned = 0);
        box& add(const std::vector<box_pack>&);
        box& add(const std::initializer_list<widget>&);
        box& add(const widget&);
        box& setHomogeneous(bool = true);

    private:
        GtkBox* inst;
    };
};

#endif