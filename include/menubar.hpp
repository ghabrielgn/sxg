#ifndef GUI_MENUBAR_HPP
#define GUI_MENUBAR_HPP

#include "container.hpp"
#include "menu_item.hpp"

namespace gui {
    class menubar : public gui::container {
    public:
        menubar(GtkWidget* widget) {
            obj = widget;
        }

        menubar() : menubar(gtk_menu_bar_new()) {}

        menubar& add(const std::string& title, const std::initializer_list<basic_pack>& list) {
            auto mainItem = gtk_menu_item_new_with_mnemonic(title.c_str());
            auto menu = gtk_menu_new();
            gtk_menu_item_set_submenu(GTK_MENU_ITEM(mainItem), menu);
            for (auto pack : list) {
                gui::menu_item item(pack.text);
                gtk_menu_shell_append(GTK_MENU_SHELL(menu), item.object());
                //item.signal("activate", pack);
                gui::signal(GTK_MENU_ITEM(item.object()), "activate", pack);
            }
            gtk_menu_shell_append(GTK_MENU_SHELL(obj), mainItem);
            return *this;
        }
    };
};

#endif