#ifndef GUI_LIST_HPP
#define GUI_LIST_HPP

#include <vector>
#include "list_row.hpp"
#include "widget.hpp"

namespace gui {
    class list : public gui::container {
    public:
        list(GtkWidget* widget) {
            obj = widget;
            inst = GTK_LIST_BOX(obj);
        }

        list() : list(gtk_list_box_new()) {}

        list& add(const std::string& labelText) {
            return add(gui::label(labelText));
        }

        list& add(const widget& widget) {
            gtk_list_box_insert(inst, widget.object(), -1);
            widget.show();

            auto row = gtk_list_box_get_row_at_index(inst, rows.size());
            rows.push_back(row);
            return *this;
        }

        list& operator<<(const std::string& labelText) {
            return add(labelText);
        }

        list& operator<<(const widget& widget) {
            return add(widget);
        }

        list_row& operator[](unsigned index) {
            return rows[index];
        }

    private:
        GtkListBox* inst;
        std::vector<list_row> rows;
    };
};

#endif