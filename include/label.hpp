#ifndef GUI_LABEL_HPP
#define GUI_LABEL_HPP

#include "widget.hpp"

namespace gui {
    class label : public gui::widget {
    public:
        label(GtkWidget* widget) {
            obj = widget;
            inst = GTK_LABEL(obj);
        }

        label(const std::string& text = "") : label(gtk_label_new(text.c_str())) {}

        void setText(const std::string& text) {
            gtk_label_set_text(inst, text.c_str());
        }

    private:
        GtkLabel* inst;
    };
};

#endif