#ifndef GUI_HPP
#define GUI_HPP

#include <gtk/gtk.h>
#include <string>
#include <vector>

#include "widget.hpp"
#include "container.hpp"

namespace gui {
    const auto SHADOW_NONE = GTK_SHADOW_NONE;
    const auto SHADOW_IN = GTK_SHADOW_IN;
    const auto SHADOW_OUT = GTK_SHADOW_OUT;
    const auto SHADOW_ETCHED_IN = GTK_SHADOW_ETCHED_IN;
    const auto SHADOW_ETCHED_OUT = GTK_SHADOW_ETCHED_OUT;

    struct basic_pack {
        basic_pack(const std::string& text, void (*fn)())
        : text(text), basic_fn(fn), arg_fn(NULL), widget(NULL) {}
        basic_pack(const std::string& text, void (*fn)(GtkWidget*), const gui::widget& widget)
        : text(text), basic_fn(NULL), arg_fn(fn), widget(&widget) {}
        const std::string& text;
        void (*basic_fn)();
        void (*arg_fn)(GtkWidget*);
        const gui::widget* widget;
    };

    namespace {
        void hierarchy_aux(std::vector<container>& list, const gui::widget& last) {
            for (unsigned i = 1; i < list.size(); i++) {
                list[i - 1].add(list[i]);
            }
            list.back().add(last);
        }

        void hierarchy_aux(std::vector<container>& list) {
            if (false) (void)hierarchy_aux(list); // suppressing a possible "unused function" warning
            auto& last = list.back();
            list.pop_back();
            hierarchy_aux(list, last);
        }

        template<typename... Args>
        void hierarchy_aux(std::vector<container>& list, const gui::container& parent, Args... others) {
            list.push_back(parent);
            hierarchy_aux(list, others...);
        }
    }

    template<typename T>
    void signal(const T& widget, const std::string& type, const basic_pack& pack) {
        if (pack.basic_fn) {
            g_signal_connect(widget, type.c_str(), G_CALLBACK(pack.basic_fn), NULL);
        } else {            
            g_signal_connect(widget, type.c_str(), G_CALLBACK(pack.arg_fn), (*pack.widget).object());
        }
    }

    void init(int argc, char** argv) {
        gtk_init(&argc, &argv);
    }

    void main() {
        gtk_main();
    }

    void quit() {
        gtk_main_quit();
    }

    void close(GtkWidget* widget) {
        gtk_widget_destroy(widget);
    }

    template<typename... Args>
    void hierarchy(const container& parent, Args... others) {
        std::vector<container> list;
        list.reserve(sizeof...(Args) + 1);
        list.push_back(parent);
        hierarchy_aux(list, others...);
    }
};

#include "box.hpp"
#include "button.hpp"
#include "canvas.hpp"
#include "dialog.hpp"
#include "frame.hpp"
#include "grid.hpp"
#include "input.hpp"
#include "label.hpp"
#include "list_row.hpp"
#include "list.hpp"
#include "menubar.hpp"
#include "menu_item.hpp"
#include "scroll_area.hpp"
#include "window.hpp"

#endif