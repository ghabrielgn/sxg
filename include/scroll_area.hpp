#ifndef GUI_SCROLL_AREA_HPP
#define GUI_SCROLL_AREA_HPP

#include "container.hpp"

namespace gui {
    class scroll_area : public gui::container {
    public:
        scroll_area(GtkWidget* widget) {
            obj = widget;
            inst = GTK_SCROLLED_WINDOW(obj);
        }

        scroll_area(unsigned width = 200, unsigned height = 200, const GtkShadowType& shadow = gui::SHADOW_NONE)
        : scroll_area(gtk_scrolled_window_new(NULL, NULL)) {
            gtk_scrolled_window_set_shadow_type(inst, shadow);
            setSize(width, height);
        }

        scroll_area& setSize(unsigned width, unsigned height) {
            gtk_scrolled_window_set_min_content_width(inst, width);
            gtk_scrolled_window_set_min_content_height(inst, height);
            return *this;
        }

    private:
        GtkScrolledWindow* inst;
    };
};

#endif