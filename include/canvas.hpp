#ifndef GUI_CANVAS_HPP
#define GUI_CANVAS_HPP

#include <cmath>
#include <initializer_list>
#include <unordered_map>
#include "widget.hpp"

namespace gui {
    class canvas : public gui::widget {
    public:
        canvas(GtkWidget* widget) {
            obj = widget;
        }
        canvas(unsigned = 200, unsigned = 200);
        void setSize(unsigned, unsigned);
        cairo_t* cairo() const;
        void setColor(unsigned, unsigned, unsigned, double = 1);
        void fill(bool = true);
        void stroke(bool = true);
        void queue();
        void moveTo(double, double);
        void strokeRect(double, double, unsigned, unsigned);
        void fillRect(double, double, unsigned, unsigned);
        void strokePolygon(const std::initializer_list<std::pair<double, double>>&);
        void fillPolygon(const std::initializer_list<std::pair<double, double>>&);
        void arc(double, double, unsigned, double = 0, double = 2 * M_PI, bool = true);
        void strokeCircle(double, double, unsigned);
        void fillCircle(double, double, unsigned);
        void strokeArc(double, double, unsigned, double, double, bool = true);

    private:
        void buildRectangle(double x, double y, unsigned width, unsigned height);
        void buildPolygon(const std::initializer_list<std::pair<double, double>>& pairs);
    };
};

#endif